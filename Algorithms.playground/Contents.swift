import UIKit



public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init() { self.val = 0; self.next = nil; }
    public init(_ val: Int) { self.val = val; self.next = nil; }
    public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }
}

extension ListNode {
    func printList() {
        if next != nil {
            print("\(val) -> ")
        } else { print(val) }
        next?.printList()
    }
}

class Algorithms {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var dict: [Int: Int] = [:]
        for (i, n) in nums.enumerated() {
            let rest = target - n
            if let e = dict[rest] {
                return [e, i]
            } else {
                dict[n] = i
            }
        }
        return []
    }
    
    func containsDuplicate(_ nums: [Int]) -> Bool {
        var set = Set<Int>()
        for i in nums {
            if !set.insert(i).inserted { return true }
        }
        return false
    }
    
    func maxProfit(_ prices: [Int]) -> Int {
        var min = Int.max
        var maxProfit = 0
        for i in prices {
            if i < min { min = i }
            else if i - min > maxProfit {
                maxProfit = i-min
            }
        }
        return maxProfit
    }
    
    func isAnagram(_ s: String, _ t: String) -> Bool {
        guard s.count == t.count else { return false }
        var map = [Character : Int]()
        for (c1, c2) in zip(s,t) {
            map[c1, default: 0] += 1
            map[c2, default: 0] -= 1
        }
        return map.reduce(0, { $0 + abs($1.value) }) == 0
    }
    
    func isValid(_ s: String) -> Bool {
        var stack = [Character]()
        for i in s {
            if "({[".contains(i) { stack.append(i) }
            else if !stack.isEmpty {
                if i == ")" && stack.removeLast() != "(" { return false }
                else if i == "}" && stack.removeLast() != "{" { return false }
                else if i == "]" && stack.removeLast() != "[" { return false }
            } else { return false }
        }
        return stack.isEmpty
    }
    
//    func productExceptSelf(_ nums: [Int]) -> [Int] {
//        var res = nums
//        for i in 0..<res.count {
//            if let n = dict[i] {
//                dict[i] = n*nums[i]
//            }
//        }
//        return dict.values as? [Int] ?? []
//    }
    
    var carry = 0
    
    func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
        if l1 == nil && l2 == nil && carry == 0 { return nil }
                
        let sum = (l1?.val ?? 0) + (l2?.val ?? 0) + carry
        carry = sum / 10
                
        return ListNode(sum % 10, addTwoNumbers(l1?.next, l2?.next))
    }
    
    func maxArea(_ height: [Int]) -> Int {
        var maxArea = 0
        var l = 0
        var r = height.count - 1
        while l < r {
            maxArea = max(maxArea, min(height[l], height[r]) * (r - l))
            if height[l] < height[r] {
                l += 1
            } else {
                r -= 1
            }
        }
        return maxArea
    }
    
    func trap(_ height: [Int]) -> Int {
        let n = height.count
        if n < 3 { return 0 }
        var totalWater = 0
        var i = 0
        var j = n - 1
        var lMax = height[i]
        var rMax = height[j]
        while i <= j {
            lMax = max(lMax, height[i])
            rMax = max(rMax, height[j])
            if lMax <= rMax {
                totalWater += lMax - height[i]
                i += 1
            } else {
                totalWater += rMax - height[j]
                j -= 1
            }
        }
        return totalWater
    }
}

let a = Algorithms()

//a.maxArea([1,8,6,2,5,4,8,3,7])
//a.trap([0,1,0,2,1,0,1,3,2,1,2,1])
//a.twoSum([2,7,11,15], 9)
//a.containsDuplicate([1,2,3,4])
//a.isAnagram("anagram", "nagaram")
//a.isValid("()")

//var l13 = ListNode(3, nil)
//var l12 = ListNode(4, l13)
//var l11 = ListNode(2, l12)
//
//var l23 = ListNode(4, nil)
//var l22 = ListNode(6, l23)
//var l21 = ListNode(5, l22)
//
//_ = a.addTwoNumbers(l11, l21)?.printList()
